#!/usr/bin/env bash
echo '-----> Cleaning project'

./gradlew clean

echo '-----> Build & upload project to BinTray'

./gradlew clean build bintrayUpload -PbintrayUser=zadmobileteam -PbintrayKey=c70b40e2064336ffc42d49154199d08531bedf7d -PdryRun=false
~
