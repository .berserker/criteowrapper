package dev.anhndt.myapplication

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.criteo.publisher.*
import com.criteo.publisher.model.AdSize
import com.criteo.publisher.model.AdUnit
import com.criteo.publisher.model.BannerAdUnit
import com.criteo.publisher.model.InterstitialAdUnit
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    private var mAdsInterstitial: CriteoInterstitial? = null
    private var mInterstitialAdUnit: InterstitialAdUnit? = null

    private var mCriteoBannerView: CriteoBannerView? = null
    private var mBannerAdUnit: BannerAdUnit? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLoadBannerAd.setOnClickListener {
            setText("Loading...")
            loadBannerAd()
        }


        btnLoadAd.setOnClickListener {
            setText("Loading...")
            loadInterstitialAd()
        }

        GetGAIDTask().execute()
    }

    private fun loadInterstitialAd() {

        val pubId = try {
            etPubId.text.toString().trim()
        } catch (e: Exception) {
            "B-058123"
        }

        val zoneId = try {
            etInterstitialZoneId.text.toString().trim()
        } catch (e: Exception) {
            "Mobile_Interstitial"
        }

        mInterstitialAdUnit = InterstitialAdUnit(zoneId)

        val adUnits = ArrayList<AdUnit>()
        adUnits.add(mInterstitialAdUnit!!)

        Criteo.init(application, pubId, adUnits)

        mAdsInterstitial = CriteoInterstitial(MainActivity@ this, mInterstitialAdUnit)
        mAdsInterstitial?.setCriteoInterstitialAdListener(object : CriteoInterstitialAdListener {
            override fun onAdClicked() {
                setText("onAdClicked")
            }

            override fun onAdClosed() {
                setText("onAdClosed")
            }

            override fun onAdFailedToReceive(p0: CriteoErrorCode?) {
                setText("onAdFailedToReceive $p0")
            }

            override fun onAdLeftApplication() {
                setText("onAdLeftApplication")
            }

            override fun onAdOpened() {
                setText("onAdOpened")
            }

            override fun onAdReceived() {
                setText("onAdReceived")
            }
        })

        mAdsInterstitial?.setCriteoInterstitialAdDisplayListener(object :
            CriteoInterstitialAdDisplayListener {
            override fun onAdFailedToDisplay(p0: CriteoErrorCode?) {
                setText("onAdFailedToDisplay $p0")
            }

            override fun onAdReadyToDisplay() {
                setText("onAdReadyToDisplay")
                if (mAdsInterstitial!!.isAdLoaded) {
                    mAdsInterstitial?.show()
                }
            }
        })

        val bidResponse = Criteo.getInstance().getBidResponse(mInterstitialAdUnit)
        val bidToken: BidToken? = if (bidResponse != null && bidResponse.isBidSuccess) {
            val bidPrice = bidResponse.price
            bidResponse.bidToken
        } else {
            null
        }

        if (bidToken != null) {
            mAdsInterstitial?.loadAd(bidToken)
        } else {
            mAdsInterstitial?.loadAd()
        }
    }

    private fun loadBannerAd() {

        val pubId = try {
            etPubId.text.toString().trim()
        } catch (e: Exception) {
            "B-058123"
        }

        val zoneId = try {
            etZoneId.text.toString().trim()
        } catch (e: Exception) {
            "Mobile_Banner_300x250"
        }


        mBannerAdUnit = BannerAdUnit(zoneId, AdSize(300, 250))

        val adUnits = ArrayList<AdUnit>()
        adUnits.add(mBannerAdUnit!!)

        Criteo.init(application, pubId, adUnits)

        mCriteoBannerView = CriteoBannerView(this, mBannerAdUnit)
        mCriteoBannerView?.setCriteoBannerAdListener(object : CriteoBannerAdListener {
            override fun onAdClicked() {
                setText("onAdClicked")
            }

            override fun onAdClosed() {
                setText("onAdClosed")
            }

            override fun onAdFailedToReceive(p0: CriteoErrorCode?) {
                setText("onAdFailedToReceive $p0")
            }

            override fun onAdLeftApplication() {
                setText("onAdLeftApplication")
            }

            override fun onAdOpened() {
                setText("onAdOpened")
            }

            override fun onAdReceived(p0: View?) {
                setText("onAdReceived $p0")
            }
        })

        mCriteoBannerView?.loadAd()
        if (cbvBannerAdContainer.childCount != 0) {
            cbvBannerAdContainer.removeAllViews()
        }

        cbvBannerAdContainer.addView(mCriteoBannerView)
    }

    private fun setText(text: String) {
        tvStatus.text = text
    }

    override fun onDestroy() {
        super.onDestroy()
        mCriteoBannerView?.destroy()
    }

    private inner class GetGAIDTask : AsyncTask<String, Int, String>() {

        override fun doInBackground(vararg strings: String): String {
            var adInfo: AdvertisingIdClient.Info?
            adInfo = null
            try {
                adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this@MainActivity)
                if (adInfo!!.isLimitAdTrackingEnabled)
                // check if user has opted out of tracking
                    return "did not found GAID... sorry"
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            }

            return adInfo!!.id
        }

        override fun onPostExecute(s: String) {
            Log.d(TAG, s)
            tvAaid.text = s
        }
    }
}
