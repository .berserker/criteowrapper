-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService

-keepattributes *Annotation*

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# Gson specific classes
-keep class sun.misc.Unsafe { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

# keep library android v4
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-dontwarn android.support.v4.**

## Adtima
#-keep public class dev.anhndt.myapplication.*
#-keep public class dev.anhndt.myapplication.**
#-keep public class dev.anhndt.myapplication.** {
#  public protected *;
#}
#-keep public class dev.anhndt.criteopublishersdkwrapper.*
#-keep public class dev.anhndt.criteopublishersdkwrapper.**
#-keep public class dev.anhndt.criteopublishersdkwrapper.** {
#  public protected *;
#}

# Google
-keep class com.google.android.gms.ads.** { *; }
-keep interface com.google.android.gms.ads.** { *; }
-dontwarn com.google.android.gms.ads.**

-keep class com.google.ads.interactivemedia.** { *; }
-keep interface com.google.ads.interactivemedia.** { *; }
-dontwarn com.google.ads.interactivemedia.**

# Criteo
-keepattributes InnerClasses

-keep class **.R
-keep class **.R$* {
    <fields>;
}

-keepattributes InnerClasses
 -keep class **.R
 -keep class **.R$* {
    <fields>;
}

-keepclassmembers class **.R$layout {
    public static <fields>;
}

-keepclassmembers class **.string {
    public static <fields>;
}

-keep class com.criteo.**{ *; }
-keep class com.criteo.publisher.**{ *; }
-keep interface com.criteo.publisher.**{ *; }
-keep class com.criteo.publisher.model.**{ *; }
-keep interface com.criteo.publisher.model.**{ *; }

-dontwarn com.criteo.**
-dontwarn com.criteo.publisher.**

-keep class dev.anhndt.**{ *; }
-keep interface dev.anhndt.**{ *; }

-dontwarn com.anhndt.**


-dontwarn org.apache.**
-dontwarn java.**
-dontwarn javax.**